# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#Overall hypothesis: We need to use greater than/less than
#to determine the max value. Use an if statement

def max_of_three(value1, value2, value3):
    maximum = 0
    return maximum

#input 3 integers
#output 1 integer
#We want to define a function that will give us the largest integer
#out of a set of three

input_one = 10
input_two = 100
input_three = 7

result = max_of_three(input_one, input_two, input_three)
print("result:", result)
